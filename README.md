# segmentnervecell

attempting to detect, segment and count red cores from the nerve cell picture with MATLAB.

this project:

* extracts red color from chosen picture and counts them
* adjusts histogram for grayscale analysis
* applies threshold to create binary image
* does morphological operations on grayscale and binary images
* transforms distance and watershed
* analysis by using regionprops
