%% Copyright Sepehr Mohseni


close all; clear all; clc;

%% black and white blob
a = zeros(30,50);
a(10:20, 6:40) = 1;
a(10:14, 20:25) = 0;
a(16:20, 20:25) = 0;
a(12:19, 36:40) = 0;
a(15, 40) = 1;
a(15, 30) = 0;
a(14:16, 6:14) = 0;
a(9,  8) = 1;
a(9,  10) = 1;
a(10, 12) = 0;
a(10, 16) = 0;
figure, subplot(4,1,1), imshow(a, 'InitialMagnification', 'fit');
title('Original', 'FontSize', 12, 'color', 'red')

%% morphological strel structure
se = strel('square', 3); %creates 3x3 structure

se_image = padarray(ones(3), [13 23]);
subplot(4,1,2), imshow(se_image, 'InitialMagnification','fit')
title('Morphological Structure', 'FontSize', 12, 'color', 'red')

%% erosion 
b = imerode(a, se);
subplot(4,1,3), imshow(b,  'InitialMagnification', 'fit')
title('Original->Erosion', 'FontSize', 12, 'color', 'red')

%% dilation
c = imdilate(b, se);
subplot(4,1,4), imshow(c,'InitialMagnification', 'fit')
title('Erosion->Dilation', 'FontSize', 12, 'color', 'red')

