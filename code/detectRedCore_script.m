%% Detecting red core 

%% Copyright Sepehr Mohseni
% 
clear all; 
close all; 
clc;       

%% Loading a bitmap image 
Iorg = imread('.\redcore_large.bmp');
figure, imshow(Iorg, 'InitialMagnification', 70);
text(size(Iorg, 2), size(Iorg,1)+15, ...
    '摜?F?@Ɨ?s?@?l?w? ]Ȋw??Z^?[', ...
    'FontSize', 8, ...
    'HorizontalAlignment', 'right');
text(size(Iorg, 2), size(Iorg,1)+35, ...
    'Image courtesy of RIKEN Brain Science Institute', ...
    'FontSize', 8, ...
    'HorizontalAlignment', 'right');

%% show red color
Ired = Iorg(:,:,1);   
imshow(Ired, [], 'InitialMagnification', 70); title('Red only Intensity');

%% adjusting histogram
figure;
subplot(3,2,1), imshow(Ired, []), title('ORIGINAL'), 
subplot(3,2,2), imhist(Ired);
subplot(3,2,3), imshow(histeq(Ired), []), title('Histogram Equalization');
subplot(3,2,4), imhist(histeq(Ired));
subplot(3,2,5), imshow(adapthisteq(Ired), []), title('Contrast Limited Adaptive Histogram Equalization');
subplot(3,2,6), imhist(adapthisteq(Ired));

%% cut threshold.

Ired_bw_try = Ired > 50;
figure, imshow(Ired_bw_try, 'InitialMagnification', 70); title('binary Image');

%% morphological operation to clean up the blob

se = strel('square',7); 
Ired_close = imclose(Ired, se);
imshow(Ired_close, [], 'InitialMagnification', 70); title('Closed result');

%% thresholding the image to turn to binary
Ired_bw = Ired_close > 50;
imshow(Ired_bw, 'InitialMagnification', 70); title('binary Image');


h = figure;
subplot(2,2,1), imshow(Ired, []), title('original grayscale');
subplot(2,2,2), imshow(Ired_bw_try), title('binary image from original grayscale');
subplot(2,2,3), imshow(Ired_close, []), title('Closed grayscale');
subplot(2,2,4), imshow(Ired_bw), title('binary image from closed grayscale');

%% filling open holes
Ired_bw = imfill(Ired_bw, 'holes');
figure, imshow(Ired_bw, 'InitialMagnification', 70); title('binary Image with holes filled');

%% area open to eliminate small objects
Ired_bw = bwareaopen(Ired_bw, 200);
imshow(Ired_bw, 'InitialMagnification', 70); title('binary Image w/ area open to 200 pixels or above');

%% calculate distance transform 
Ired_dist = -bwdist(~Ired_bw);
imshow(Ired_dist, [], 'InitialMagnification', 70); title('Distance transform');

%% force background as basin
Ired_dist(~Ired_bw) = -Inf;
Ired_lim = imhmin(Ired_dist, 2); %limit over segmentation
imshow(Ired_lim, [], 'InitialMagnification', 70); title('Background forced to basin');

%% use Watershed to segment blobs
Ired_seg = watershed(Ired_lim);
imshow(label2rgb(Ired_seg,'jet'), 'InitialMagnification', 70);

%% use region prop to draw rectangles, and counts
figure, imshow(Iorg, 'InitialMagnification', 70);
hold on;
status = regionprops(Ired_seg,'Centroid','BoundingBox');

for x = 1:length(status)
    xy = status(x).Centroid;
    z = status(x).BoundingBox;
    rectangle('Position',z, 'EdgeColor', 'g');
    text(xy(1),xy(2),'*', 'Color', 'b');
end
numblobs = num2str(size(status,1) - 1);
text(255,500, ['counted red cores: ', numblobs], 'FontSize', 14, 'color', 'red');
hold off;
